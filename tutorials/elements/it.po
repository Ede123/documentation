msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Elements of Design\n"
"POT-Creation-Date: 2021-03-28 20:01+0200\n"
"PO-Revision-Date: 2016-06-11 13:53+0100\n"
"Last-Translator: Firas Hanife <FirasHanife@gmail.com>\n"
"Language-Team: FirasHanife@gmail.com\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.4\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "Firas Hanife <FirasHanife@gmail.com>, 2014, 2015, 2016"

#. (itstool) path: articleinfo/title
#: tutorial-elements.xml:6
msgid "Elements of design"
msgstr "Elementi di grafica"

#. (itstool) path: articleinfo/subtitle
#: tutorial-elements.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-elements.xml:11
msgid ""
"This tutorial will demonstrate the elements and principles of design which "
"are normally taught to early art students in order to understand various "
"properties used in art making. This is not an exhaustive list, so please "
"add, subtract, and combine to make this tutorial more comprehensive."
msgstr ""
"Questo tutorial mostra gli elementi e i principi di grafica normalmente "
"insegnati ai nuovi studenti d'arte per poter comprendere le proprietà "
"utilizzate nella produzione artistica. Questa lista non è esaustiva, quindi "
"è consigliato aggiungere, rimuovere e combinare il materiale per rendere "
"questo tutorial il più completo possibile."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:25
msgid "Elements of Design"
msgstr "Elementi di grafica"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:26
msgid "The following elements are the building blocks of design."
msgstr ""
"I seguenti elementi sono i blocchi fondamentali della progettazione grafica."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:30 elements-f01.svg:127
#, no-wrap
msgid "Line"
msgstr "Linea"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:31
msgid ""
"A line is defined as a mark with length and direction, created by a point "
"that moves across a surface. A line can vary in length, width, direction, "
"curvature, and color. Line can be two-dimensional (a pencil line on paper), "
"or implied three-dimensional."
msgstr ""
"Una linea è definita come un segno con una lunghezza e una direzione, creata "
"da un punto che si muove lungo una superficie. Una linea può variare in "
"lunghezza, larghezza, direzione, curvatura e colore. La linea può essere "
"bidimensionale (una matita su carta) o implicitamente tridimensionale."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:45 elements-f01.svg:140
#, no-wrap
msgid "Shape"
msgstr "Forma"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:46
msgid ""
"A flat figure, shape is created when actual or implied lines meet to "
"surround a space. A change in color or shading can define a shape. Shapes "
"can be divided into several types: geometric (square, triangle, circle) and "
"organic (irregular in outline)."
msgstr ""
"Una figura piatta è una forma creata quando delle linee si incontrano per "
"racchiudere uno spazio. Un cambiamento di colore o sfumatura può definire "
"una forma. Le forme possono essere suddivise in diverse tipologie: "
"geometriche (quadrato, triangolo, cerchio) e organiche (dal tracciato "
"irregolare)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:60 elements-f01.svg:192
#, no-wrap
msgid "Size"
msgstr "Dimensione"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:61
msgid ""
"This refers to variations in the proportions of objects, lines or shapes. "
"There is a variation of sizes in objects either real or imagined."
msgstr ""
"Questa si riferisce alle variazioni nelle proporzioni degli oggetti, linee o "
"forme. Si ha una variazione di dimensioni in oggetti sia reali che "
"immaginari."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:74 elements-f01.svg:153
#, no-wrap
msgid "Space"
msgstr "Spazio"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:75
msgid ""
"Space is the empty or open area between, around, above, below, or within "
"objects. Shapes and forms are made by the space around and within them. "
"Space is often called three-dimensional or two- dimensional. Positive space "
"is filled by a shape or form. Negative space surrounds a shape or form."
msgstr ""
"Lo spazio è l'area vuota o aperta tra, intorno, sopra, sotto, o all'interno "
"di oggetti. Le forme sono fatte dallo spazio intorno e dentro di loro. Lo "
"spazio è spesso chiamato tridimensionale o bidimensionale. Lo spazio "
"positivo è riempito da una forma. Lo spazio negativo circonda una forma."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:89 elements-f01.svg:114
#, no-wrap
msgid "Color"
msgstr "Colore"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:90
msgid ""
"Color is the perceived character of a surface according to the wavelength of "
"light reflected from it. Color has three dimensions: HUE (another word for "
"color, indicated by its name such as red or yellow), VALUE (its lightness or "
"darkness), INTENSITY (its brightness or dullness)."
msgstr ""
"Il colore è il carattere percepito da una superficie secondo la lunghezza "
"d'onda della luce riflessa da essa. Il colore ha tre dimensioni: HUE "
"(un'altra parola per il colore, indicato con il suo nome come il rosso o "
"giallo), il valore (la sua luminosità o l'oscurità), l'intensità (la sua "
"luminosità o opacità)."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:104 elements-f01.svg:166
#, no-wrap
msgid "Texture"
msgstr "Texture"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:105
msgid ""
"Texture is the way a surface feels (actual texture) or how it may look "
"(implied texture). Textures are described by word such as rough, silky, or "
"pebbly."
msgstr ""
"La texture è il modo in cui si sente una superficie al tatto (texture reale) "
"o come appare (texture implicita). Le texture sono descritte con parole come "
"ruvida, vellutata o ciottolosa."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:118 elements-f01.svg:179
#, no-wrap
msgid "Value"
msgstr "Valore"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:119
msgid ""
"Value is how dark or how light something looks. We achieve value changes in "
"color by adding black or white to the color. Chiaroscuro uses value in "
"drawing by dramatically contrasting lights and darks in a composition."
msgstr ""
"Il valore è quanto scuro o quanto chiaro appare qualcosa. Per cambiare il "
"valore si aggiunge nero o bianco al colore. Il chiaroscuro usa dei valori "
"che aumentano molto la differenza tra il chiaro e lo scuro della composizone."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:133
msgid "Principles of Design"
msgstr "Principi di grafica"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:134
msgid "The principles use the elements of design to create a composition."
msgstr ""
"I principi di utilizzo degli oggetti per la creazione di composizioni "
"grafiche."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:138 elements-f01.svg:205
#, no-wrap
msgid "Balance"
msgstr "Equilibrio"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:139
msgid ""
"Balance is a feeling of visual equality in shape, form, value, color, etc. "
"Balance can be symmetrical or evenly balanced or asymmetrical and un-evenly "
"balanced. Objects, values, colors, textures, shapes, forms, etc., can be "
"used in creating a balance in a composition."
msgstr ""
"L'equilibrio è una sensazione di equilibrio tra forme, colori, ecc. "
"L'equilibrio può essere simmetrico o uniformemente bilanciato o asimmetrico "
"e irregolarmente bilanciato. Oggetti, colori, texture, forme, ecc. possono "
"essere utilizzate per creare un equilibrio nella composizione."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:153 elements-f01.svg:218
#, no-wrap
msgid "Contrast"
msgstr "Contrasto"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:154
msgid "Contrast is the juxtaposition of opposing elements"
msgstr "Il contrasto è l'affiancamento di oggetti ben differenti."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:166 elements-f01.svg:231
#, no-wrap
msgid "Emphasis"
msgstr "Enfasi"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:167
msgid ""
"Emphasis is used to make certain parts of their artwork stand out and grab "
"your attention. The center of interest or focal point is the place a work "
"draws your eye to first."
msgstr ""
"L'enfasi è utilizzata per rendere alcune parti del prodotto più visibili in "
"modo da catturare l'attenzione. Il punto di interesse è l'area che colpisce "
"maggiormante a prima vista."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:180 elements-f01.svg:244
#, no-wrap
msgid "Proportion"
msgstr "Proporzione"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:181
msgid ""
"Proportion describes the size, location or amount of one thing compared to "
"another."
msgstr ""
"La proporzione descrive la dimensione, la posizione o la quantità di un "
"oggetti rispetto ad un altro."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:193 elements-f01.svg:257
#, no-wrap
msgid "Pattern"
msgstr "Motivo"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:194
msgid ""
"Pattern is created by repeating an element (line, shape or color) over and "
"over again."
msgstr ""
"Il motivo è creato ripetendo un oggetto (linea, forma o colore) più volte."

#. (itstool) path: sect2/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:206 elements-f01.svg:271
#, no-wrap
msgid "Gradation"
msgstr "Gradazione"

#. (itstool) path: sect2/para
#: tutorial-elements.xml:207
msgid ""
"Gradation of size and direction produce linear perspective. Gradation of "
"color from warm to cool and tone from dark to light produce aerial "
"perspective. Gradation can add interest and movement to a shape. A gradation "
"from dark to light will cause the eye to move along a shape."
msgstr ""
"La gradazione di dimensione e direzione produce una prospettiva lineare. La "
"gradazione del colore da caldo a freddo e della tonalità da scura a chiara "
"produce una prospettiva aerea. Dallo scuro al chiaro produce un effetto di "
"profondità. Aggiunge quindi valore e movimento alla forma."

#. (itstool) path: sect1/title
#. (itstool) path: text/tspan
#: tutorial-elements.xml:222 elements-f01.svg:284
#, no-wrap
msgid "Composition"
msgstr "Composizione"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:223
msgid "The combining of distinct elements to form a whole."
msgstr "La combinazione di vari oggetti per creare un insieme."

#. (itstool) path: sect1/title
#: tutorial-elements.xml:235
msgid "Bibliography"
msgstr "Bibliografia"

#. (itstool) path: sect1/para
#: tutorial-elements.xml:236
msgid "This is a partial bibliography used to build this document."
msgstr "Qui trovi la bibliografia utilizzata per la creazione del documento."

#. (itstool) path: listitem/para
#: tutorial-elements.xml:241
msgid ""
"<ulink url=\"http://www.makart.com/resources/artclass/EPlist.html\">http://"
"www.makart.com/resources/artclass/EPlist.html</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:246
msgid ""
"<ulink url=\"http://www.princetonol.com/groups/iad/Files/elements2.htm"
"\">http://www.princetonol.com/groups/iad/Files/elements2.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:251
msgid ""
"<ulink url=\"http://www.johnlovett.com/test.htm\">http://www.johnlovett.com/"
"test.htm</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:256
msgid ""
"<ulink url=\"http://digital-web.com/articles/elements_of_design/\">http://"
"digital-web.com/articles/elements_of_design/</ulink>"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-elements.xml:261
msgid ""
"<ulink url=\"http://digital-web.com/articles/principles_of_design/\">http://"
"digital-web.com/articles/principles_of_design/</ulink>"
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-elements.xml:266
msgid ""
"Special thanks to Linda Kim (<ulink url=\"http://www.coroflot.com/redlucite/"
"\">http://www.coroflot.com/redlucite/</ulink>) for helping me (<ulink url="
"\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) with this tutorial. "
"Also, thanks to the Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) and the graphics "
"people have submitted to that project."
msgstr ""
"Un ringraziamento a Linda Kim (<ulink url=\"http://www.coroflot.com/"
"redlucite/\">http://www.coroflot.com/redlucite/</ulink>) per l'aiuto (<ulink "
"url=\"http://www.rejon.org/\">http://www.rejon.org/</ulink>) nella creazione "
"di questo tutorial, alla Open Clip Art Library (<ulink url=\"http://www."
"openclipart.org/\">http://www.openclipart.org/</ulink>) e alle persone che "
"hanno realizzato il materiale."

#. (itstool) path: Work/format
#: elements-f01.svg:48 elements-f02.svg:48 elements-f03.svg:48
#: elements-f04.svg:48 elements-f05.svg:48 elements-f06.svg:91
#: elements-f07.svg:91 elements-f08.svg:206 elements-f09.svg:48
#: elements-f10.svg:48 elements-f11.svg:48 elements-f12.svg:445
#: elements-f13.svg:102 elements-f14.svg:173 elements-f15.svg:452
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: elements-f01.svg:88
#, no-wrap
msgid "Elements"
msgstr "Elementi"

#. (itstool) path: text/tspan
#: elements-f01.svg:101
#, no-wrap
msgid "Principles"
msgstr "Principi"

#. (itstool) path: text/tspan
#: elements-f01.svg:297
#, no-wrap
msgid "Overview"
msgstr "Panoramica"

#. (itstool) path: text/tspan
#: elements-f04.svg:79
#, no-wrap
msgid "BIG"
msgstr "GR"

#. (itstool) path: text/tspan
#: elements-f04.svg:93
#, no-wrap
msgid "small"
msgstr "piccolo"

#. (itstool) path: text/tspan
#: elements-f12.svg:710
#, no-wrap
msgid "Random Ant &amp; 4WD"
msgstr "Formica &amp; 4WD"

#. (itstool) path: text/tspan
#: elements-f12.svg:721
#, no-wrap
msgid "SVG  Image Created by Andrew Fitzsimon"
msgstr "SVG creato da Andrew Fitzsimon"

#. (itstool) path: text/tspan
#: elements-f12.svg:726
#, no-wrap
msgid "Courtesy of Open Clip Art Library"
msgstr "Per gentile concessione di Open Clip Art Library"

#. (itstool) path: text/tspan
#: elements-f12.svg:731
#, no-wrap
msgid "http://www.openclipart.org/"
msgstr "http://www.openclipart.org/"

#~ msgid "http://www.makart.com/resources/artclass/EPlist.html"
#~ msgstr "http://www.makart.com/resources/artclass/EPlist.html"

#~ msgid "http://www.princetonol.com/groups/iad/Files/elements2.htm"
#~ msgstr "http://www.princetonol.com/groups/iad/Files/elements2.htm"

#~ msgid "http://www.johnlovett.com/test.htm"
#~ msgstr "http://www.johnlovett.com/test.htm"

#~ msgid "http://digital-web.com/articles/elements_of_design/"
#~ msgstr "http://digital-web.com/articles/elements_of_design/"

#~ msgid "http://digital-web.com/articles/principles_of_design/"
#~ msgstr "http://digital-web.com/articles/principles_of_design/"
